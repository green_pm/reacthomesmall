import React from 'react';
import Link from '../Components/router-link'
import {RectRound} from '../Components/rects'
import {
  MdMusicVideo,
  MdPanorama,
  MdPeople,
  MdFolderShared,
  MdExitToApp } from 'react-icons/md';

const styles = {
  root: {
    padding: '.5em'
  },
  label: {
    fontFamily: 'Avenir,Helvetica,Arial,sansSerif',
    textDecoration: 'none',
    color: '#0009',
    fontWeight: "bold",
    padding: '.5em'
  },
};

function Header() {
    return (
      <RectRound className="header row row-space-between gray">
        <div className="header-left row col-center">
          <Link to="/" >
            <img src="logo.png" alt="logo" width="30em" height="30em"/>
          </Link>
          <Link to="/" style={styles.label} >Домашний сайт</Link>
        </div>
        <div className="header-right row col-center" style={styles.right}>
          <Link to='#'       ><MdMusicVideo   /></Link>
          <Link to="/fotos"  ><MdPanorama     /></Link>
          <Link to="/roles"  ><MdPeople       /></Link>
          <Link to="/folders"><MdFolderShared /></Link>
          <Link to='#'       ><MdExitToApp    /></Link>
        </div>
      </RectRound>
    );
  }
  
export default Header;
  