import React from 'react';
import {Links}  from '../Components/list-of-links'
import {RectRound} from '../Components/rects'

const links = [
    { name: 'VK', link: 'https://vk.com/dzerzhinsk' },
    { name: 'FB', link: 'https://fb.com/a.a.grekhov' },
    { name: 'HH', link: 'https://nn.hh.ru/applicant/resumes/view?resume=e0802340ff02646c600039ed1f787763736f66' },
];

function Footer() {
  return (
    <RectRound className="footer row row-center gray-light">
      <Links links={links} />
    </RectRound>
  );
}

export default Footer;
  