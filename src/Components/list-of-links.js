import React from 'react';

const styles = {
  ul: {
    listStyleType: "none",
    padding: 0,
    margin: '.3em' },
  li: {
    display: "inline-block",
    margin: "0 10px" },
  a: {
    fontWeight: "bold",
    textDecoration: 'none',
    color: "#999" }
};

export const Links = ({ links }) => {
  return (
    <ul style={styles.ul}>
      { links.map( link => <Link link={link} key={link.name} /> )}
    </ul> );
};

export const Link = ({ link }) => {
  return (
    <li style={styles.li}>
      <a href={link.link} style={styles.a}>
        {link.name}
      </a>
    </li> );
};