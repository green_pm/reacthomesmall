import React from 'react';
import {Link} from 'react-router-dom';

const styles = {
  root: {
    color: '#0008',
    fontSize: 'xx-large',
    padding: '.2em 0.3em',
  }
};

function RouterLink ({to, style, children}) {
  if (typeof style === 'object' && style !== null)
    return (
        <Link className="router-link column col-bottom" to={to} style={style}>{children}</Link>
    );
  else 
  return (
      <Link className="router-link column col-bottom" to={to} style={styles.root}>{children}</Link>
  );
}

export default RouterLink;