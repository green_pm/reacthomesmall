import React from 'react';
import styled from '@emotion/styled'

const Div = styled('div')`
  contain: content;
`

export const Rect = (props) => {
  return (
  <Div className={'Rect ' + props.className} style={props.style}>
      {props.children}
  </Div>
  );
}

const RectBase = styled(Rect)`
  margin: .2em;
  border: 1px solid #0001;
  border-radius: 0.5rem;
`

export const RectRound = (props) => {
  return (
  <RectBase className={'RectRound ' + props.className} style={props.style}>
      {props.children}
  </RectBase>
  );
}