import React from 'react';

import { Routes, Route } from "react-router-dom";
import {RectRound} from '../Components/rects'
import First from './first';
import Login from './login';
import Fotos from './fotos';
import Folders from './folders-editor';
import Roles from './roles-editor';

function Body() {
    return (
      <RectRound className="body">
        <Routes>
          <Route path="/" element={<First />} />
          <Route path="login" element={<Login />} />
          <Route path="fotos" element={<Fotos />} />
          <Route path="roles" element={<Roles />} />
          <Route path="folders" element={<Folders />} />
        </Routes>
      </RectRound>
    );
  }
  
export default Body;
  