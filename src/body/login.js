import React from 'react';
import styled from '@emotion/styled'

import {Rect, RectRound} from '../Components/rects'

const RectStyle = styled(Rect)`
  max-width: 600px;
  margin-left: auto;
  margin-right: auto;
`
function Login() {
    return (
      <RectStyle className="login column row-center">
        <RectRound className='row row-center gray-light'>Вход на сайт</RectRound>
        <Rect className="row row-center">
          <h1>~ = страница авторизации = ~</h1>
        </Rect>
      </RectStyle>
    );
  }
  
export default Login;
