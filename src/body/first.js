import React from 'react';
import styled from '@emotion/styled'

import {Rect} from '../Components/rects'
import Link from '../Components/router-link';

const RectStyle = styled(Rect)`
  color: #155724;
  background-color: #d4edda;
  border-color: #c3e6cb;
  padding: 0.75rem 1.25rem;
`

function First() {
    return (
      <RectStyle className="first column col-center">
        <h1>~ = домашний сайт = ~</h1>
        <Rect className='column col-center'>
          <label>для продолжения работы необходимо</label>
          <Rect>
            <Link to={"/login"} style={{}}>авторизоваться или зарегистрироваться</Link>
          </Rect>
        </Rect>
        <p>
            авторизация пройдена
        </p>
      </RectStyle>
    );
  }

export default First;
