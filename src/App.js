import './app.css';
import React from 'react';

import { BrowserRouter } from "react-router-dom";

import Header from "./header/header";
import Body   from "./body/body";
import Footer from "./footer/footer";

export default function App() {
  return (
    <div className="app">
      <BrowserRouter>
        <Header />  
        <Body />
        <Footer />
      </BrowserRouter>
    </div>
  );
}


